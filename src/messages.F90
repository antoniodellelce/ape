!! Copyright (C) 2004-2012 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
!! $Id: messages.F90 778 2013-07-11 15:49:39Z micael $

#include "global.h"

module messages_m
  use global_m
  use oct_parser_m
  use utilities_m
  implicit none


                    !---Global Variables---!

  !Default unit numbers
  integer :: stderr = 0
  integer :: stdin  = 5
  integer :: stdout = 6

  !Array were the messages are stored
  character(len=80)  :: message(200)

  !The verbosity level
  integer :: verbose = 0

  !Verbosity levels of warning and normal messages
  integer, parameter :: WARNING = 0,  &
                        NORMAL  = 10, &
                        DEBUG   = 1000

  !Debug stuff
  integer, parameter :: M_STACK_SIZE = 50
  character(len=40)  :: sub_stack(M_STACK_SIZE)
  real(r8)           :: time_stack(M_STACK_SIZE)
  integer            :: no_sub_stack = 0

  integer, public :: s_epoch_sec, s_epoch_usec


                    !---Public/Private Statements---!

  private
  public :: messages_init, &
            message, &
            write_fatal, &
            write_warning, &
            write_info, &
            print_date, &
            push_sub, &
            pop_sub, &
            assert_die, &
            stderr, &
            stdin, &
            stdout

contains

  subroutine messages_init()
    !-----------------------------------------------------------------------!
    ! Initializes the messages: gets the init time, the vervose level and   !
    ! checks if the code is in debug mode or not.                           !
    !-----------------------------------------------------------------------!

    ! Get epoch time
    call sys_gettimeofday(s_epoch_sec, s_epoch_usec)

    !Read verbose
    call oct_parse_int('Verbose', 0, verbose)

    !Are we in debug mode?
    if (verbose >= DEBUG) then
      in_debug_mode = .true.
    else
      in_debug_mode = .false.
    end if

  end subroutine messages_init

  subroutine write_fatal(no_lines)
    !-----------------------------------------------------------------------!
    ! Writes a message and stops the programm.                              !
    !                                                                       !
    !  no_lines -  number of lines to write                                 !
    !-----------------------------------------------------------------------!
    integer, intent(in) :: no_lines

    integer :: i

    write(stderr,'(//"Fatal Error (description follows)")')
    do i = 1, no_lines
      write(stderr,'(2X, A)') trim(message(i))
    end do
    write(stderr,*)
    stop

  end subroutine write_fatal

  subroutine write_warning(no_lines)
    !-----------------------------------------------------------------------!
    ! If the verbosity level is greater than the WARNING level it writes a  !
    ! warning message.                                                      !
    !                                                                       !
    !  no_lines -  number of lines to write                                 !
    !-----------------------------------------------------------------------!
    integer, intent(in) :: no_lines

    integer :: i

    if (verbose > WARNING) then
      do i = 1, no_lines
        write(stdout, '(A)') "Warning: "//trim(message(i))
      end do
    end if

  end subroutine write_warning

  subroutine write_info(no_lines, verbose_limit, unit)
    !-----------------------------------------------------------------------!
    ! If the verbosity level is greater than the NORMAL level it writes a   !
    ! message.                                                              !
    !                                                                       !
    !  no_lines      -  number of lines to write                            !
    !  verbose_limit - if present, only writes the message if the verbosity !
    !                  level is greater than the verbose_limit              !
    !  unit          - if present, writes the message to unit, instead of   !
    !                  stdout, without taking into account the verbosity.   !
    !-----------------------------------------------------------------------!
    integer, intent(in) :: no_lines
    integer, intent(in), optional :: verbose_limit
    integer, intent(in), optional :: unit

    integer :: i

    if (present(unit)) then
      do i = 1, no_lines
        write(unit, '(A)') trim(message(i))
      end do
    else
      if (verbose > NORMAL) then
        do i = 1, no_lines
          if (.not.present(verbose_limit)) then
            write(stdout, '(A)') trim(message(i))
          else if (verbose > verbose_limit) then
            write(stdout, '(A)') trim(message(i))
          end if
        end do
      end if
    end if

  end subroutine write_info

  subroutine assert_die(s, f, l)
    !-----------------------------------------------------------------------!
    ! Write a message about a failed assertion and stops the program.       !
    !                                                                       !
    !  s - failed assertion                                                 !
    !  f - number of the line where the assertion failed                    !
    !  l - name of the file where the assertion failed                      !
    !-----------------------------------------------------------------------!
    character(len=*), intent(in) :: s, f
    integer, intent(in) :: l

    write(message(1), '(2a)') 'Assertion "', trim(s)
    write(message(2), '(a,i5)') 'failed in line ', l
    write(message(3), '(3a)') 'in file "', f, '"'
    call write_fatal(3)

  end subroutine assert_die

  subroutine print_date(str)
    !-----------------------------------------------------------------------!
    ! Prints a string followed by the date and time. The format is the      !
    ! following: " 'str' YYYY/MM/DD at HH:MM "                              !
    !                                                                       !
    !  str - string to be written                                           !
    !-----------------------------------------------------------------------!
    character(len = *), intent(in) :: str

    integer :: val(8)

    call date_and_time(values=val)
    message(1) = ""
    write(message(3),'(a,i4,a1,i2.2,a1,i2.2,a,i2.2,a1,i2.2,a1,i2.2)') &
      str , val(1), "/", val(2), "/", val(3), &
      " at ", val(5), ":", val(6), ":", val(7)
    message(2) = str_center(trim(message(3)), 70)
    message(3) = ""
    call write_info(3)

  end subroutine print_date

  subroutine time_diff(sec1, usec1, sec2, usec2)
    !-----------------------------------------------------------------------!
    ! Computes t2 <- t2-t1. sec1,2 and usec1,2 are seconds, microseconds of !
    ! t1,2                                                                  !
    !-----------------------------------------------------------------------!
    integer, intent(in)    :: sec1, usec1
    integer, intent(inout) :: sec2, usec2

    ! Correct overflow.
    if(usec2 - usec1 < 0) then
      usec2 = 1000000 + usec2
      if(sec2 >= sec1) then
        sec2 = sec2-1
      end if
    end if

    ! Replace values.
    if(sec2 >= sec1) then
      sec2 = sec2-sec1
    end if
    usec2 = usec2-usec1

  end subroutine time_diff

  subroutine push_sub(sub_name)
    !-----------------------------------------------------------------------!
    ! This routine should be called at the very beginning of other          !
    ! subroutines. When runnint in debug mode it prints the name of the     !
    ! subroutine, the time and the memory used.                             !
    ! It also places the name of the subroutine in a stack. The stack is    !
    ! used by subroutine pop_sub.                                           !
    !                                                                       !
    ! sub_name - string with the name of the subroutine.                    !
    !-----------------------------------------------------------------------!
    character(len=*), intent(in) :: sub_name

    integer i, sec, usec

    if (verbose < DEBUG) return

    call sys_gettimeofday(sec, usec)
    call time_diff(s_epoch_sec, s_epoch_usec, sec, usec)

    no_sub_stack = no_sub_stack + 1
    if(no_sub_stack > 49) then
      sub_stack(M_STACK_SIZE) = 'push_sub'
      write(message(1),"('Too many recursion levels (max=)',I3)"), M_STACK_SIZE
      call write_fatal(1)
    else
      sub_stack(no_sub_stack)  = trim(sub_name)
      time_stack(no_sub_stack) = sys_clock()

      write(stderr,'(a,i6,a,i6.6,f12.6,i8, a)', advance='no') "* I ", &
        sec,'.',usec, &
        sys_clock()/1e6_r8, &
        sys_getmem(), " | "
      do i = no_sub_stack-1, 1, -1
        write(stderr,'(a)', advance='no') "..|"
      end do
      write(stderr,'(a)') trim(sub_name)
    end if

  end subroutine push_sub

  subroutine pop_sub()
    !-----------------------------------------------------------------------!
    ! This routine should be called at the very end of other subroutines.   !
    ! When running in debug mode it prints the name of the last subroutine  !
    ! in the stack, the time, the time spent and the memory used.           !
    !-----------------------------------------------------------------------!
    integer i, sec, usec

    if (verbose < DEBUG) return

    call sys_gettimeofday(sec, usec)
    call time_diff(s_epoch_sec, s_epoch_usec, sec, usec)

    if(no_sub_stack > 0) then
      write(stderr,'(a,i6,a,i6.6,f12.6,i8, a)', advance='no') "* O ", &
        sec,'.',usec, &
        (sys_clock()-time_stack(no_sub_stack))/1e6_r8, &
        sys_getmem(), " | "
      do i = no_sub_stack-1, 1, -1
        write(stderr,'(a)', advance='no') "..|"
      end do
      write(stderr,'(a)') trim(sub_stack(no_sub_stack))

      no_sub_stack = no_sub_stack - 1
    else
      no_sub_stack = 1
      sub_stack(1) = 'pop_sub'
      message(1) = 'Too few recursion levels'
      call write_fatal(1)
    end if

  end subroutine pop_sub

end module messages_m
