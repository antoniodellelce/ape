/*
 Copyright (C) 2004-2007 M. Oliveira, F. Nogueira

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.

 $Id: utilities_c.c 778 2013-07-11 15:49:39Z micael $
*/

#include <config.h>

#include <stdio.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>

#include "string_f.h"

/*  These interface routines were taken from the octopus code 
 (htpp://www.tddft.org/programs/octopus).
 Only the names were changed. */

void FC_FUNC_(sys_mkdir, SYS_MKDIR)
                 (STR_F_TYPE name STR_ARG1)
{
  struct stat buf;
  char *name_c;

  name_c = TO_C_STR1(name);
  if(!*name_c || stat(name_c, &buf) == 0) return;
  mkdir(name_c, 0775);
  free(name_c);
}

void FC_FUNC_(sys_rm, SYS_RM)
                 (STR_F_TYPE name STR_ARG1)
{
  char *name_c;

  name_c = TO_C_STR1(name);
  unlink(name_c);
  free(name_c);
}

void FC_FUNC_(sys_getcwd, SYS_GETCWD)
                 (STR_F_TYPE name STR_ARG1)
{
  char s[256];

  getcwd(s, 256);
  TO_F_STR1(s, name);
}

double FC_FUNC_(sys_clock, SYS_CLOCK)
       ()
{
  return (double) clock();
}


void FC_FUNC_(sys_gettimeofday, SYS_GETTIMEOFDAY)
  (int *sec, int *usec)
{
#ifdef HAVE_GETTIMEOFDAY
  struct timeval tv;
  struct timezone tz;

  gettimeofday(&tv, &tz);

  /* The typecast below should use long. However, this causes
     incompatibilities with Fortran integers.  Using int will cause
     wrong results when tv.tv_sec exceeds INT_MAX=2147483647 */
  *sec  = (int) tv.tv_sec;
  *usec = (int) tv.tv_usec;
/*
  char str[sizeof("HH:MM:SS")];
  time_t local;
  local = tv.tv_sec;
  strftime(str, sizeof(str), "%T", localtime(&local));
  printf("%s.%06ld \n", str, (long) tv.tv_usec);
  printf("%ld.%06ld \n", (long) tv.tv_sec, (long) tv.tv_usec);
*/
#else
  *sec  = 0;
  *usec = 0; 
#endif
}


int FC_FUNC_(sys_getmem, SYS_GETMEM)
     ()
{
#ifdef linux
  static size_t pagesize = 0;
  FILE *f;
  int pid;
  long mem;
  char s[256];
  
  if(pagesize == 0)
    pagesize = sysconf(_SC_PAGESIZE);
  
  pid = getpid();
  sprintf(s, "%s%d%s", "/proc/", pid, "/statm");
  if((f = fopen(s, "r")) == NULL) return -1;
  fscanf(f, "%lu", &mem);
  fclose(f);
  
  return (mem*pagesize) >> 10;
#else
  return -1;
#endif
}
