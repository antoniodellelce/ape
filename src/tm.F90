!! Copyright (C) 2004-2011 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
!! $Id: tm.F90 778 2013-07-11 15:49:39Z micael $

#include "global.h"

module troullier_martins_m
  use global_m
  use messages_m
  use utilities_m
  use output_m
  use mesh_m
  use multiroot_m
  use quantum_numbers_m
  use potentials_m
  use wave_equations_m
  use troullier_martins_equations_m
  implicit none


                    !---Public/Private Statements---!

  private
  public :: tm_gen, &
            tm_equations_rhs, &
            tm_solve_system


contains

  subroutine tm_gen(qn, ev, wave_eq, rel, tol, m, ae_potential, rc, ps_v, ps_wf, ps_wfp)
    !-----------------------------------------------------------------------!
    ! Generates the pseudo wave-functions and the corresponding             !
    !pseudo-potential using the Troullier-Martins scheme.                   !
    !                                                                       !
    !  qn           - quantum numbers of valence state                      !
    !  occ          - occupation of valence state                           !
    !  ev           - eigenvalue of valence state                           !
    !  wave_eq      - wave equation to use                                  !
    !  rel          - if true, use the relativistic extension to the scheme !
    !  tol          - tolerance                                             !
    !  m            - mesh                                                  !
    !  ae_potential - all-electron potential                                !
    !  rc           - cutoff radius                                         !
    !  integrator   - integrator object                                     !
    !  ps_v         - pseudo-potential on the mesh                          !
    !  ps_wf        - all-electron/pseudo wavefunction                      !
    !  ps_wfp       - all-electron/pseudo wavefunction derivative           !
    !                                                                       !
    ! On input qn, ps_wf and ps_wfp contain the all-electron                !
    ! quantum-numbers and wavefunctions and on exit the pseudo              !
    ! wavefunctions and quantum numbers.                                    !
    !-----------------------------------------------------------------------!
    type(qn_t),         intent(inout) :: qn
    real(R8),           intent(in)    :: ev
    integer,            intent(in)    :: wave_eq
    logical,            intent(in)    :: rel
    real(R8),           intent(in)    :: tol
    type(mesh_t),       intent(in)    :: m
    type(potential_t),  intent(in)    :: ae_potential
    real(R8),           intent(in)    :: rc
    real(R8),           intent(out)   :: ps_v(m%np)
    real(R8),           intent(inout) :: ps_wf(:,:)  ! ps_wf(m%np, wf_dim)
    real(R8),           intent(inout) :: ps_wfp(:,:) ! ps_wfp(m%np, wf_dim)

    integer :: wf_dim, i
    real(R8) :: rhs(7), c(7)
    real(R8) :: wf_rc, wfp_rc, norm
    real(R8), allocatable :: ae_wf(:,:), ae_wfp(:,:)
    type(mesh_t) :: mr

    call push_sub("tm_gen")

    !
    call mesh_null(mr)

    !Write core radius
    write(message(1),'(4x,"Core radius:",1x,f7.3,5x)') rc
    call write_info(1,20)
    call write_info(1,unit=info_unit("pp"))

    !Copy all-electron wavefunctions
    wf_dim = qn_wf_dim(qn)
    allocate(ae_wf(m%np, wf_dim), ae_wfp(m%np, wf_dim))
    ae_wf = ps_wf
    ae_wfp = ps_wfp

    !Wavefunctions and norm at rc
    wf_rc  = rc*mesh_extrapolate(m, ae_wf(:,1), rc)
    wfp_rc = rc*mesh_extrapolate(m, ae_wfp(:,1), rc) + wf_rc/rc
    if (wf_rc < M_ZERO) then
      ae_wf  = -ae_wf
      ae_wfp = -ae_wfp
      wf_rc  = -wf_rc
      wfp_rc = -wfp_rc
    end if
    if (rel) then
      norm = mesh_integrate(m, sum(ae_wf**2, dim=2), b=rc)
    else
      select case (wave_eq)
      case (SCHRODINGER, SCALAR_REL)
        norm = mesh_integrate(m, ae_wf(:,1)**2, b=rc)
      case (DIRAC)
        norm = mesh_integrate(m, ae_wf(:,1)**2, b=rc) + mesh_integrate(m, ae_wf(:,2)**2)
      end select
    end if

    !Get right hand side of equations to solve
    call tm_equations_rhs(m, rc, rel, qn, ev, wf_rc, wfp_rc, norm, ae_potential, rhs)

    !New mesh
    mr = m
    call mesh_truncate(mr, rc)

    !Solve system of equations
    call tm_solve_system(mr, rc, rel, qn, ev, rhs, tol, c, ps_wf(1:mr%np,:), ps_wfp(1:mr%np,:), ps_v(1:mr%np), .false.)

    !Get the pseudo-wavefunctions and set the quantum numbers to their correct value
    qn%n = qn%l + 1
    ps_wf(mr%np:m%np,1:wf_dim) = ae_wf(mr%np:m%np,1:wf_dim)
    ps_wfp(mr%np:m%np,1:wf_dim) = ae_wfp(mr%np:m%np,1:wf_dim)
    if (wave_eq == DIRAC .and. .not. rel) then
      ps_wf(mr%np:m%np,2) = M_ZERO
      ps_wfp(mr%np:m%np,2) = M_ZERO
    end if
    do i = mr%np, m%np
      ps_v(i) = v(ae_potential, m%r(i), qn)
    end do

    !
    deallocate(ae_wf, ae_wfp)
    call mesh_end(mr)

    call pop_sub()
  end subroutine tm_gen

  subroutine tm_equations_rhs(m, rc, rel, qn, ev, grc, gprc, norm, ae_potential, rhs)
    !-----------------------------------------------------------------------!
    ! Returns the right-hand side of the TM set of non-linear equations.    !
    !                                                                       !
    !  m            - mesh                                                  !
    !  rc           - cutoff radius                                         !
    !  rel          - if true, use the relativistic extension to the scheme !
    !  qn           - quantum numbers of valence state                      !
    !  ev           - eigenvalue of valence state                           !
    !  grc          - wave-function at rc                                   !
    !  gprc         - first derivative of the wave-function at rc           !
    !  norm         - norm of the wave-functions up to rc                   !
    !  ae_potential - all-electron potential                                !
    !  rhs          - right-hand side of equations                          !
    !-----------------------------------------------------------------------!
    type(mesh_t),      intent(in)  :: m
    real(R8),          intent(in)  :: rc, ev, grc, gprc, norm
    logical,           intent(in)  :: rel
    type(qn_t),        intent(in)  :: qn
    type(potential_t), intent(in)  :: ae_potential
    real(R8),          intent(out) :: rhs(7)

    integer :: i
    real(R8) :: vrc, vprc, vpprc, vppprc, delta, deltap, deltapp, deltappp, k, alpha, alphap, alphapp
    real(R8), allocatable :: tmp(:)

    call push_sub("tm_equations_rhs")

    !Potential at rc
    vrc   = v(ae_potential, rc, qn)
    vprc  = dvdr(ae_potential, rc, qn)
    vpprc = d2vdr2(ae_potential, rc, qn)

    !Get right hand side of equations to solve
    if (rel) then
      allocate(tmp(m%np))
      do i = 1, m%np
        tmp(i) = dvdr(ae_potential, m%r(i), qn)
      end do
      vppprc = mesh_extrapolate(m, mesh_derivative(m, tmp), rc)
      deallocate(tmp)
      delta    = (vrc - ev)/M_TWO/M_C2
      deltap   = vprc/M_TWO/M_C2
      deltapp  = vpprc/M_TWO/M_C2
      deltappp = vppprc/M_TWO/M_C2

      rhs(1)  = log(grc/rc**(qn%l+1))
      rhs(2)  = gprc/grc - (real(qn%l,r8) + M_ONE)/rc
      k       = -M_TWO*(qn%j - real(qn%l,R8))*(qn%j + M_HALF)
      alpha   = rhs(2) + (real(qn%l,R8) + M_ONE + k)/rc
      rhs(3)  =   M_TWO*(M_ONE - delta)*(vrc - ev) - rhs(2)**2 &
                - M_TWO*(real(qn%l,r8) + M_ONE)/rc*rhs(2) &
                - deltap*alpha/(M_ONE - delta)
      alphap  = rhs(3) - (real(qn%l,R8) + M_ONE + k)/rc**2
      rhs(4)  =   M_TWO*(M_ONE - delta)*vprc &
                - M_TWO*deltap*(vrc - ev) &
                - M_TWO*rhs(2)*rhs(3) &
                + M_TWO*(real(qn%l,r8) + M_ONE)/rc**2*rhs(2) &
                - M_TWO*(real(qn%l,r8) + M_ONE)/rc*rhs(3) &
                - deltapp*alpha/(M_ONE - delta) &
                - deltap**2*alpha/(M_ONE - delta)**2 &
                - deltap*alphap/(M_ONE - delta)
      alphapp = rhs(4) + M_TWO*(real(qn%l,R8) + M_ONE + k)/rc**3
      rhs(5)  =   M_TWO*(M_ONE - delta)*vpprc &
                - M_FOUR*deltap*vprc &
                - M_TWO*deltapp*(vrc - ev) &
                - M_TWO*rhs(2)*rhs(4) &
                - M_TWO*rhs(3)**2 &
                - M_FOUR*(real(qn%l,r8) + M_ONE)/rc**3*rhs(2) &
                + M_FOUR*(real(qn%l,r8) + M_ONE)/rc**2*rhs(3) &
                - M_TWO*(real(qn%l,r8) + M_ONE)/rc*rhs(4) &
                - deltappp*alpha/(M_ONE - delta) &
                - M_THREE*deltap*deltapp*alpha/(M_ONE - delta)**2 &
                - M_TWO*deltap**3*alpha/(M_ONE - delta)**3 &
                - M_TWO*deltapp*alphap/(M_ONE - delta) &
                - M_TWO*deltapp**2*alphap/(M_ONE - delta)**2 &
                - deltap*alphapp/(M_ONE - delta)

    else
      rhs(1) = log(grc/rc**(qn%l+1))
      rhs(2) =   gprc/grc &
               - (real(qn%l,r8) + M_ONE)/rc
      rhs(3) =   M_TWO*(vrc - ev) &
               - M_TWO*(real(qn%l,r8) + M_ONE)/rc*rhs(2) &
               - rhs(2)**2
      rhs(4) =   M_TWO*vprc &
               + M_TWO*(real(qn%l,r8) + M_ONE)/rc**2*rhs(2) &
               - M_TWO*(real(qn%l,r8) + M_ONE)/rc*rhs(3) &
               - M_TWO*rhs(2)*rhs(3)
      rhs(5) =   M_TWO*vpprc &
               - M_FOUR*(real(qn%l,r8) + M_ONE)/rc**3*rhs(2) &
               + M_FOUR*(real(qn%l,r8) + M_ONE)/rc**2*rhs(3) &
               - M_TWO*(real(qn%l,r8) + M_ONE)/rc*rhs(4) &
               - M_TWO*rhs(3)**2 &
               - M_TWO*rhs(2)*rhs(4)
    end if

    rhs(6) = M_ZERO    
    rhs(7) = norm

    call pop_sub()
  end subroutine tm_equations_rhs

  subroutine tm_solve_system(m, rc, rel, qn, ev, rhs, tol, c, ps_wf, ps_wfp, ps_v, silent)
    !-----------------------------------------------------------------------!
    ! Solve the TM set of non-linear equations.                             !
    !                                                                       !
    !  m            - mesh                                                  !
    !  rc           - cutoff radius                                         !
    !  rel          - if true, use the relativistic extension to the scheme !
    !  qn           - quantum numbers of valence state                      !
    !  ev           - eigenvalue of valence state                           !
    !  rhs          - right-hand side of equations                          !
    !  tol          - tolerance                                             !
    !  c            - coeficients of the polynomial                         !
    !  ps_wf        - pseudo wavefunction                                   !
    !  ps_wfp       - pseudo wavefunction derivative                        !
    !  ps_v         - pseudo-potential                                      !
    !-----------------------------------------------------------------------!
    type(mesh_t), intent(in)  :: m
    real(R8),     intent(in)  :: rc, ev, rhs(7), tol
    logical,      intent(in)  :: rel, silent 
    type(qn_t),   intent(in)  :: qn
    real(R8),     intent(out) :: c(7)
    real(R8),     intent(out) :: ps_wf(:,:)  ! ps_wf(m%np, wf_dim)
    real(R8),     intent(out) :: ps_wfp(:,:) ! ps_wfp(m%np, wf_dim)
    real(R8),     intent(out) :: ps_v(m%np)

    real(R8) :: cc(2), f(2)
    type(multiroot_solver_t) :: mr_solver

    call push_sub("tm_solve_system")

    !Guess
    cc = M_ZERO

    !Allocate memory
    call tm_equations_init(m, rc, rel, qn, ev, rhs, silent)

    !Solve non-linear system
    call multiroot_solver_init(2, M_ZERO, tol, M_ZERO, 300, BROYDEN, mr_solver)
    call multiroot_solve_system(mr_solver, cc, tm_equations, tm_equations_write_info)
    c(1) = cc(1)
    c(7) = cc(2)
    call tm_solve_linear_system(c)

    !Compute the final pseudo-wavefunction and the pseudopotential
    call tm_ps_wavefunctions(c, ps_wf, ps_wfp, ps_v)

    !Write final results
    if (.not. silent) then
      call tm_equations(2, cc, f)
      write(message(1),'(4x,"Coefficients:")')
      write(message(2),'(6x,"c0  =",1x,es16.9e2,3x,"c2  =",1x,es16.9e2)') c(1), c(2)
      write(message(3),'(6x,"c4  =",1x,es16.9e2,3x,"c6  =",1x,es16.9e2)') c(3), c(4)
      write(message(4),'(6x,"c8  =",1x,es16.9e2,3x,"c10 =",1x,es16.9e2)') c(5), c(6)
      write(message(5),'(6x,"c12 =",1x,es16.9e2)') c(7)
      write(message(6),'(4x,"Residues:")')
      write(message(7),'(6X,"c2**2 + c4(2l+5)    =",1x,ES16.9e2)') f(1)
      write(message(8),'(6X,"norm[AE] - norm[PP] =",1x,ES16.9e2)') f(2)
      message(9) = ""
      call write_info(9,20)
      call write_info(9,unit=info_unit("pp"))
    end if

    !Free memory
    call tm_equations_end()

    call pop_sub()
  end subroutine tm_solve_system

end module troullier_martins_m
