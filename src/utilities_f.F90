!! Copyright (C) 2004-2012 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
!! $Id: utilities_f.F90 778 2013-07-11 15:49:39Z micael $

module utilities_m
  use global_m
  implicit none


                    !---Interfaces---!

  interface
    subroutine sys_getcwd(name)
      character(len=*), intent(out) :: name
    end subroutine sys_getcwd
    
    subroutine sys_mkdir(name)
      character(len=*), intent(in) :: name
    end subroutine sys_mkdir

    subroutine sys_rm(name)
      character(len=*), intent(in) :: name
    end subroutine sys_rm

    function sys_clock()
      real(8) :: sys_clock
    end function sys_clock

    subroutine sys_gettimeofday(sec, usec)
      integer, intent(out) :: sec, usec
    end subroutine sys_gettimeofday

    integer function sys_getmem()
    end function sys_getmem
  end interface


                    !---Public/Private Statements---!

  private
  public :: locate, &
            str_center, &
            sys_getcwd, &
            sys_mkdir, &
            sys_rm, &
            sys_clock, &
            sys_gettimeofday, &
            sys_getmem

contains

  function locate(xx, x, guess)
    !-----------------------------------------------------------------------!
    ! Given an array xx and a value x, returns locate so x is between       !
    ! xx(locate) and xx(locate+1). It may use a guess value for locate.     !
    ! If guess <= 0 or > size xx then the guess is ignored.                 !
    !                                                                       !
    !  xx    - array of values where x is to be located. Must be monotonic  !
    !  x     - value we want to locate                                      !
    !  guess - guess value for locate                                       !
    !-----------------------------------------------------------------------!
    real(R8), intent(in) :: x
    real(R8), intent(in) :: xx(:)
    integer,  intent(in) :: guess
    integer :: locate
    
    real(R8) :: s
    integer :: n, nr, nm, i, nl
     
    n = size(xx)
    s = sign(M_ONE, xx(n)-xx(1))

    !Check if x is out of bounds
    if(s*(xx(n) - x) < M_ZERO) then
      locate = n
      return
    elseif(s*(xx(1) - x) > M_ZERO) then
      locate = 0
      return
    end if
   
    if ( guess <= 0 .or. guess > n) then !Dont hunt. Just do bissection
      nl = 0
      nr = n + 1
    else !Start hunting
      i = 1
      if (s*(xx(guess)-x) <= M_ZERO) then
        nl=guess
        do
          if (nl == n) nl = n - 1
          nr = nl + i
          if(nr > n) nr = n
          if (s*(xx(nr) - x) >= M_ZERO) then
            exit
          else
            nl = nr
          end if
          i = 2*i
        end do
      elseif (s*(xx(guess) - x) > M_ZERO) then
        nr = guess
        do
          nl = nr - i
          if(nl < 1) nl = 1
          if (s*(xx(nl) - x) <= M_ZERO) then
            exit
          else
            nr = nl
          end if
          i = 2*i
        end do
      end if
    end if

    !Bissection
    do
      if(nr - nl == 1) then
        locate = nl
        exit
      end if
      nm = (nl + nr)/2
      if (s*(xx(nm) - x) < M_ZERO) then
        nl = nm
      elseif (s*(xx(nm) - x) > M_ZERO) then
        nr = nm
      else 
        locate = nm
        exit
      end if
    end do

  end function locate

  function str_center(s_in, l)
    !-----------------------------------------------------------------------!
    !  Center a string.                                                     !
    !                                                                       !
    !  s_in - string to center                                              !
    !  l    - length of the centered string                                 !
    !-----------------------------------------------------------------------!
    character(len=*), intent(in) :: s_in
    integer,          intent(in) :: l
    character(len=80) :: str_center

    integer :: pad, i, li

    li = len(s_in)
    if(l < li) then
      str_center(1:l) = s_in(1:l)

    else  
      pad = (l - li)/2

      str_center = ""
      do i = 1, pad
        str_center(i:i) = " ";
      end do
      str_center(pad + 1:pad + li + 1) = s_in(1:li)
      do i = pad + li + 1, l
        str_center(i:i) = " ";
      end do
    end if

  end function str_center

end module utilities_m
