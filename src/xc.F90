!! Copyright (C) 2004-2013 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
!! $Id: xc.F90 778 2013-07-11 15:49:39Z micael $

#include "global.h"

module xc_m
  use global_m
  use oct_parser_m
  use messages_m
  use units_m
  use io_m
  use output_m
  use mesh_m
  use xc_f90_lib_m
  use functionals_m
  use ps_io_m
  use states_batch_m
  implicit none


                    !---Interfaces---!

  interface assignment (=)
     module procedure xc_copy
  end interface


                    !---Derived Data Types---!

  type xc_t
    private
    integer :: nspin
    type(functional_t) :: functls(3)
    integer :: id_val
    integer :: corrections

    ! Approximation for the kinetic energy density
    integer :: ked_approximation
    type(functional_t) :: ked_functl

    !Non-linear core-corrections
    !This is not initialized in xc_init but in xc_nlcc_init
    logical  :: nlcc
    real(R8) :: rc
    integer  :: np
    real(R8), pointer :: rho_core(:)
    real(R8), pointer :: grad_core(:)
    real(R8), pointer :: lapl_core(:)
    real(R8), pointer :: tau_core(:)
  end type xc_t


                    !---Global Variables---!

  !Core-correcton scheme
  integer, parameter :: CC_NONE  = 0, &
                        CC_TM    = 1, &
                        CC_FHI   = 2

  !Corrections to the xc functionals
  integer, parameter :: XC_CORRECTION_NONE  =  0, &
                        XC_CORRECTION_REL_C =  1, &
                        XC_CORRECTION_REL_X =  2, &
                        XC_CORRECTION_SIC   =  4, &
                        XC_CORRECTION_ADSIC =  8, &
                        XC_CORRECTION_RHOXC = 16

  !Exact kinetic energy density
  integer, parameter :: XC_KED_EXACT = 0


                    !---Public/Private Statements---!

  private
  public :: xc_t, &
            xc_null, &
            xc_init, &
            xc_nlcc_init, &
            assignment(=), &
            xc_end, &
            xc_model_output_info, &
            xc_model_save, &
            xc_model_load, &
            xc_potential, &
            xc_evaluate_ked_approximation, & 
            xc_output, &
            xc_ps_io_set, &
            CC_NONE, &
            CC_TM, &
            CC_FHI, &
            XC_CORRECTION_NONE,  &
            XC_CORRECTION_REL_C, &
            XC_CORRECTION_REL_X, &
            XC_CORRECTION_SIC,   &
            XC_CORRECTION_ADSIC, &
            XC_CORRECTION_RHOXC

contains

  subroutine xc_null(xc_model)
    !-----------------------------------------------------------------------!
    ! Nullifies and sets to zero all the components of xc_model.            !
    !-----------------------------------------------------------------------!
    type(xc_t), intent(out) :: xc_model

    integer :: i

    call push_sub("xc_null")

    xc_model%nspin = 0
    do i = 1, 3
      call functional_null(xc_model%functls(i))
    end do
    xc_model%id_val = 0
    xc_model%corrections = 0
    xc_model%ked_approximation = 0
    call functional_null(xc_model%ked_functl)

    xc_model%nlcc   = .false.
    xc_model%rc     = M_ZERO
    xc_model%np     = 0
    nullify(xc_model%rho_core)
    nullify(xc_model%grad_core)
    nullify(xc_model%lapl_core)
    nullify(xc_model%tau_core)

    call pop_sub()
  end subroutine xc_null

  subroutine xc_init(nspin, xc_model)
    !-----------------------------------------------------------------------!
    ! Initializes exchange-correlation model data. The exchange-correlation !
    ! models are read from the input file.                                  !
    !-----------------------------------------------------------------------!
    integer,    intent(in)    :: nspin
    type(xc_t), intent(inout) :: xc_model

    integer :: id(2), irel

    call push_sub("xc_init")
    
    xc_model%nspin = nspin

    !Read exchange-correlation model control parameter
    !The correlation only functionals in the input file are multiplied by 1000
    call oct_parse_int('XCFunctional', XC_LDA_X + XC_LDA_C_PW*1000, xc_model%id_val)
    id(1) = xc_model%id_val/1000
    id(2) = xc_model%id_val - id(1)*1000

    !Read exchange-correlation corrections
    call oct_parse_int('XCCorrections', XC_CORRECTION_NONE, xc_model%corrections)
    
    !Initialize correlation
    irel = XC_NON_RELATIVISTIC
    if (iand(xc_model%corrections, XC_CORRECTION_REL_C) /= 0) irel = XC_RELATIVISTIC
    call functional_init(nspin, id(1), irel, xc_model%functls(1))

    !Initialize exchange
    irel = XC_NON_RELATIVISTIC
    if (iand(xc_model%corrections, XC_CORRECTION_REL_X) /= 0) irel = XC_RELATIVISTIC
    call functional_init(nspin, id(2), irel, xc_model%functls(2))

    !Are we doing SIC?
    if (iand(xc_model%corrections, XC_CORRECTION_SIC) /= 0) then
      call functional_init(nspin, XC_OEP_XC_SIC, irel, xc_model%functls(3))
    end if

    !rhoxc correction should be applied only when we have an exchange functional
    if (iand(xc_model%corrections, XC_CORRECTION_RHOXC) /= 0 .and. &
        (id(2) == 0 .and. functional_kind(xc_model%functls(1)) /= XC_EXCHANGE_CORRELATION) ) then
      message(1) = "rhoxc correction should be applied to an exchange functional"
      call write_fatal(1)
    end if

    !Approximations for kinetic energy density
    call oct_parse_int('KEDFunctional', XC_KED_EXACT, xc_model%ked_approximation)
    select case (xc_model%ked_approximation)
    case (XC_KED_EXACT)
    case default
      call functional_init(nspin, xc_model%ked_approximation, irel, xc_model%ked_functl)
    end select

    call pop_sub()
  end subroutine xc_init

  subroutine xc_nlcc_init(xc_model, m, rc, cc_scheme, core_density)
    !-----------------------------------------------------------------------!
    ! Initializes the non-local core-corrections part of the xc model.      !
    !                                                                       !
    !  xc_model     -  exchange-correlation model                           !
    !  m            -  mesh                                                 !
    !  rc           -  core cut-off radius                                  !
    !  cc_scheme    -  core-correction scheme to used. Valid options are    !
    !                  CC_TM (4th order even polynomial used in atom code   !
    !                  by Jose Luis Martins) and CC_FHI (6th order          !
    !                  polynomial used in fhi98PP by Martin Fuchs).         !
    !  core-density -  the all electron core density                        !
    !-----------------------------------------------------------------------!
    type(xc_t),   intent(inout) :: xc_model
    type(mesh_t), intent(in)    :: m
    real(R8),     intent(inout) :: rc
    integer,      intent(in)    :: cc_scheme
    real(R8),     intent(in)    :: core_density(m%np, xc_model%nspin)

    integer :: i, i_rc
    logical :: monotonous
    real(R8) :: cd_rc, cdp_rc, cdpp_rc, cdppp_rc, a, b, c, d, e, f, g, dummy, dummy1
    real(R8), allocatable :: cd(:)

    xc_model%nlcc = .true.
    xc_model%rc   = rc
    xc_model%np   = m%np
    allocate(xc_model%rho_core(m%np),  xc_model%grad_core(m%np), &
             xc_model%lapl_core(m%np), xc_model%tau_core(m%np))

    !Get first, second, and third derivatives of the core density
    allocate(cd(m%np))
    cd = sum(core_density,2)
    cd_rc   = mesh_extrapolate(m, cd, rc)
    cdp_rc  = mesh_extrapolate(m, mesh_derivative(m, cd), rc)
    cdpp_rc = mesh_extrapolate(m, mesh_derivative2(m, cd), rc)    
    cdppp_rc = mesh_extrapolate(m, mesh_derivative3(m, cd), rc)

    select case (cc_scheme)
    case (CC_TM)
      !Get parameters
      c = (rc*(cdpp_rc/cd_rc - (cdp_rc/cd_rc)**2) - cdp_rc/cd_rc)/(M_EIGHT*rc**3)
      b = (cdp_rc/cd_rc - M_FOUR*c*rc**3)/(M_TWO*rc)
      a = log(cd_rc) - b*rc**2 - c*rc**4

      !Compute partial core-density    
      do i = 1, m%np
        if (m%r(i) < rc) then
          xc_model%rho_core(i) = exp(a + b*m%r(i)**2 + c*m%r(i)**4)
        else
          xc_model%rho_core(i) = cd(i)
        end if
        if ( abs(xc_model%rho_core(i)) < M_EPSILON ) xc_model%rho_core(i)=M_ZERO
      end do

    case (CC_FHI)
      ! Insert a 4*Pi factor in the charge density and its derivatives
      cd_rc    = cd_rc*M_FOUR*M_PI
      cdp_rc   = cdp_rc*M_FOUR*M_PI
      cdpp_rc  = cdpp_rc*M_FOUR*M_PI
      cdppp_rc = cdppp_rc*M_FOUR*M_PI

      ! Find i_rc
      do i = 1, m%np
        if (m%r(i) > rc) exit
      end do
      i_rc = i

      ! Find the 5 non-zero coefficients of the polynomial
      ! Start with a = cd_rc
      a = cd_rc
      do
        ! Solve linear part
        d = M_TWENTY*(cd_rc - a)/rc**3 - M_TEN*cdp_rc/rc**2 + &
             M_TWO*cdpp_rc/rc - cdppp_rc/M_SIX
        e = (90.0_R8*(a - cd_rc) + M_FIFTY*rc*cdp_rc - M_ELEVEN*rc**2*cdpp_rc + &
             rc**3*cdppp_rc)/(M_TWO*rc**4)
        f = (72.0_R8*(cd_rc - a) - 42.0_R8*rc*cdp_rc + M_TEN*rc**2*cdpp_rc - &
             rc**3*cdppp_rc)/(M_TWO*rc**5)
        g = (M_SIXTY*(a - cd_rc) + 36.0_R8*rc*cdp_rc - M_NINE*rc**2*cdpp_rc + &
             rc**3*cdppp_rc)/(M_SIX*rc**6)

        ! Test monotonous decay
        dummy1 = a + d*m%r(i_rc)**3 + e*m%r(i_rc)**4 + &
                 f*m%r(i_rc)**5 + g*m%r(i_rc)**6
        monotonous = .true.
        do i = i_rc, 2, -1
          dummy = dummy1
          dummy1 = a + d*m%r(i-1)**3 + e*m%r(i-1)**4 + &
                   f*m%r(i-1)**5 + g*m%r(i-1)**6
          if (dummy1 < dummy) then
            monotonous = .false.
            exit
          end if
        end do
        if (monotonous) exit
        a = a*1.25_R8
      end do

      !Compute partial core-density    
      do i = 1, m%np
        if (m%r(i) < rc) then
          xc_model%rho_core(i) = (a + d*m%r(i)**3 + e*m%r(i)**4 + &
                                     f*m%r(i)**5 + g*m%r(i)**6)/M_FOUR/M_PI
        else
          xc_model%rho_core(i) = cd(i)
        end if
        if ( abs(xc_model%rho_core(i)) < M_EPSILON ) xc_model%rho_core(i) = M_ZERO
      end do

    end select

    !Higher order derivatives of the core density
    xc_model%grad_core = mesh_gradient(m, xc_model%rho_core)
    xc_model%lapl_core = mesh_laplacian(m, xc_model%rho_core)
    xc_model%tau_core = xc_model%grad_core**2/xc_model%rho_core/M_FOUR
    where(xc_model%rho_core <= 1e-30)
      xc_model%rho_core = M_ZERO
      xc_model%grad_core = M_ZERO
      xc_model%lapl_core = M_ZERO
      xc_model%tau_core = M_ZERO
    end where

    !Output info
    write(info_unit("pp"),*)
    message(1) = ""
    message(2) = "Core Corrections:"
    select case (cc_scheme)
    case (CC_TM)
      message(3) = "  Scheme: Troullier-Martins scheme" 
      write(message(4),'(2x,a,f12.6)') "rc    :", rc
      write(message(5),'(2x,a)') "Polynomial coefficients:"
      write(message(6),'(4x,a,f16.10)') "a =", a + log(M_FOUR*M_PI)
      write(message(7),'(4x,a,f16.10)') "b =", b
      write(message(8),'(4x,a,f16.10)') "c =", c
      write(message(9),'(2x,a)') "Matching at rc:"
      write(message(10),'(4x,a,f15.9,a,f15.9)') "cd(rc)   |  f0(rc)  =", &
           cd_rc,   "   | ", exp(a + b*rc**2 + c*rc**4)
      write(message(11),'(4x,a,f15.9,a,f15.9)') "cd'(rc)  |  f1(rc)  =", &
           cdp_rc,  "   | ", (M_TWO*b*rc + M_FOUR*c*rc**3)*exp(a + b*rc**2 + c*rc**4)
      write(message(12),'(4x,a,f15.9,a,f15.9)') "cd''(rc) |  f2(rc)  =", &
           cdpp_rc, "   | ", M_TWO*exp(a + b*rc**2 + c*rc**4)*(b + M_TWO*(b**2 + 3*c)*rc**2 + &
           M_EIGHT*b*c*rc**4 + M_EIGHT*c**2*rc**6)
      write(message(13),'(2x,a,f16.9)') "Integrated core charge: ", &
           M_FOUR*M_PI*mesh_integrate(m, xc_model%rho_core)
      call write_info(13,20)
      call write_info(13,unit=info_unit("pp"))

    case (CC_FHI)
      message(3) = "  Scheme: FHI scheme" 
      write(message(4),'(2x,a,f12.6)') "rc    :", rc
      write(message(5),'(2x,a)') "Polynomial coefficients:"
      write(message(6),'(4x,a,f16.10)') "a =", a
      write(message(7),'(4x,a,f16.10)') "b =", M_ZERO
      write(message(8),'(4x,a,f16.10)') "c =", M_ZERO
      write(message(9),'(4x,a,f16.10)') "d =", d
      write(message(10),'(4x,a,f16.10)') "e =", e
      write(message(11),'(4x,a,f16.10)') "f =", f
      write(message(12),'(4x,a,f16.10)') "g =", g
      write(message(13),'(2x,a)') "Matching at rc:"
      write(message(14),'(4x,a,f15.9,a,f15.9)') "cd(rc)    |  f0(rc)  =", &
           cd_rc,    "   | ", a + d*rc**3 + e*rc**4 + f*rc**5 + g*rc**6
      write(message(15),'(4x,a,f15.9,a,f15.9)') "cd'(rc)   |  f1(rc)  =", &
           cdp_rc,   "   | ", M_THREE*d*rc**2 + M_FOUR*e*rc**3 + M_FIVE*f*rc**4 + M_SIX*g*rc**5
      write(message(16),'(4x,a,f15.9,a,f15.9)') "cd''(rc)  |  f2(rc)  =", &
           cdpp_rc,  "   | ", M_SIX*d*rc + M_TWELVE*e*rc**2 + M_TWENTY*f*rc**3 + M_THIRTY*g*rc**4
      write(message(17),'(4x,a,f15.9,a,f15.9)') "cd'''(rc) |  f3(rc)  =", &
           cdppp_rc, "   | ", M_SIX*d + 24.0_r8*e*rc + M_SIXTY*f*rc**2 + 120.0_r8*g*rc**3
      write(message(18),'(2x,a,f15.9)') "Integrated core charge: ", &
           M_FOUR*M_PI*mesh_integrate(m, xc_model%rho_core)
      call write_info(18,20)
      call write_info(18,unit=info_unit("pp"))
    end select

  end subroutine xc_nlcc_init

  subroutine xc_copy(xc_out, xc_in)
    !-----------------------------------------------------------------------!
    ! Copies xc model xc_in to xc model xc_out.                             !
    !-----------------------------------------------------------------------!
    type(xc_t), intent(inout) :: xc_out
    type(xc_t), intent(in)    :: xc_in

    integer :: i

    call push_sub("xc_copy")

    call xc_end(xc_out)

    xc_out%nspin = xc_in%nspin
    do i = 1, 3
      xc_out%functls(i) = xc_in%functls(i)
    end do
    xc_out%id_val = xc_in%id_val
    xc_out%corrections = xc_in%corrections
    xc_out%ked_approximation = xc_in%ked_approximation
    xc_out%ked_functl = xc_in%ked_functl

    xc_out%nlcc  = xc_in%nlcc
    xc_out%rc    = xc_in%rc
    xc_out%np    = xc_in%np
    if (associated(xc_in%rho_core)) then
      allocate(xc_out%rho_core(xc_out%np))
      xc_out%rho_core = xc_in%rho_core
    end if
    if (associated(xc_in%grad_core)) then
      allocate(xc_out%grad_core(xc_out%np))
      xc_out%grad_core = xc_in%grad_core
    end if
    if (associated(xc_in%lapl_core)) then
      allocate(xc_out%lapl_core(xc_out%np))
      xc_out%lapl_core = xc_in%lapl_core
    end if
    if (associated(xc_in%tau_core)) then
      allocate(xc_out%tau_core(xc_out%np))
      xc_out%tau_core = xc_in%tau_core
    end if

    call pop_sub()
  end subroutine xc_copy

  subroutine xc_end(xc_model)
    !-----------------------------------------------------------------------!
    ! Frees all memory associated to the xc_model.                          !
    !-----------------------------------------------------------------------!
    type(xc_t), intent(inout) :: xc_model

    integer :: i

    call push_sub("xc_end")

    xc_model%nspin = 0
    do i = 1, 3
      call functional_end(xc_model%functls(i))
    end do
    xc_model%id_val = 0
    xc_model%corrections = 0

    xc_model%ked_approximation = 0
    call functional_end(xc_model%ked_functl)

    xc_model%nlcc = .false.
    xc_model%rc   = M_ZERO
    xc_model%np   = 0    
    if (associated(xc_model%rho_core)) deallocate(xc_model%rho_core)
    if (associated(xc_model%grad_core)) deallocate(xc_model%grad_core)
    if (associated(xc_model%lapl_core)) deallocate(xc_model%lapl_core)
    if (associated(xc_model%tau_core)) deallocate(xc_model%tau_core)

    call pop_sub()
  end subroutine xc_end

  subroutine xc_model_save(unit, xc_model)
    !-----------------------------------------------------------------------!
    ! Writes the exchange-correlation model data to a file.                 !
    !-----------------------------------------------------------------------!
    integer,    intent(in) :: unit
    type(xc_t), intent(in) :: xc_model

    integer :: i

    call push_sub("xc_model_save")

    write(unit) xc_model%nspin
    do i = 1, 3
      call functional_save(unit, xc_model%functls(i))
    end do
    write(unit) xc_model%id_val, xc_model%corrections, xc_model%ked_approximation
    call functional_save(unit, xc_model%ked_functl)

    write(unit) xc_model%nlcc
    if (xc_model%nlcc) then
      write(unit) xc_model%rc, xc_model%np
      write(unit) (xc_model%rho_core(i), i=1,xc_model%np)
      write(unit) (xc_model%grad_core(i), i=1,xc_model%np)
      write(unit) (xc_model%lapl_core(i), i=1,xc_model%np)
      write(unit) (xc_model%tau_core(i), i=1,xc_model%np)
    end if

    call pop_sub()
  end subroutine xc_model_save

  subroutine xc_model_load(unit, xc_model)
    !-----------------------------------------------------------------------!
    ! Reads the exchange-correlation model data from a file.                !
    !-----------------------------------------------------------------------!
    integer,    intent(in) :: unit
    type(xc_t), intent(inout) :: xc_model

    integer :: i

    call push_sub("xc_model_load")

    read(unit) xc_model%nspin
    do i = 1, 3
      call functional_load(unit, xc_model%functls(i))
    end do
    read(unit) xc_model%id_val, xc_model%corrections, xc_model%ked_approximation
    call functional_load(unit, xc_model%ked_functl)

    read(unit) xc_model%nlcc
    if (xc_model%nlcc) then
      read(unit) xc_model%rc, xc_model%np
      allocate(xc_model%rho_core(xc_model%np), xc_model%grad_core(xc_model%np), &
               xc_model%lapl_core(xc_model%np), xc_model%tau_core(xc_model%np))
      read(unit) (xc_model%rho_core(i), i=1,xc_model%np)
      read(unit) (xc_model%grad_core(i), i=1,xc_model%np)
      read(unit) (xc_model%lapl_core(i), i=1,xc_model%np)
      read(unit) (xc_model%tau_core(i), i=1,xc_model%np)
    end if

    call pop_sub()
  end subroutine xc_model_load

  subroutine xc_potential(xc_model, m, states, nspin, vxc, exc, vxctau)
    !-----------------------------------------------------------------------!
    ! Given a set of states, computes the corresponding                     !
    ! exchange-correlation potentials and energies.                         !
    !                                                                       !
    !  xc_model - exchange-correlation model                                !
    !  m        - mesh                                                      !
    !  nspin    - number of spin channels                                   !
    !  states   - the states                                                !
    !  vxc      - exchange-correlation potential                            !
    !  exc      - exchange-correlation energy                               !
    !  vxctau   - extra term arising from MGGA functionals                  !
    !-----------------------------------------------------------------------!
    type(xc_t),           intent(inout) :: xc_model
    type(mesh_t),         intent(in)    :: m
    integer,              intent(in)    :: nspin
    type(states_batch_t), intent(in)    :: states
    real(R8), optional,   intent(out)   :: vxc(m%np, nspin)
    real(R8), optional,   intent(out)   :: exc
    real(R8), optional,   intent(out)   :: vxctau(m%np, nspin)

    integer  :: i
    real(R8) :: eaux, ip(nspin)
    real(R8), allocatable :: e(:), v(:,:), vaux(:,:), vtau(:,:)
    real(R8), allocatable :: rho(:,:), rho_grad(:,:), rho_lapl(:,:), tau(:,:)
    real(R8), allocatable :: dv(:,:)

    call push_sub("xc_potential")

    !Allocate potential and energy work arrays
    allocate(v(m%np, xc_model%nspin), vaux(m%np, nspin), e(m%np))
    allocate(vtau(m%np, xc_model%nspin))
    e = M_ZERO; eaux = M_ZERO; v = M_ZERO; vaux = M_ZERO; vtau = M_ZERO
    if (present(exc))    exc    = M_ZERO
    if (present(vxc))    vxc    = M_ZERO
    if (present(vxctau)) vxctau = M_ZERO

    !Get total electronic density and kinetic energy density
    allocate(rho(m%np, nspin), rho_grad(m%np, nspin), rho_lapl(m%np, nspin), tau(m%np, nspin))
    rho = states_batch_density(states, nspin, m)
    rho_grad = states_batch_density_grad(states, nspin, m)
    rho_lapl = states_batch_density_lapl(states, nspin, m)
    if (xc_model%ked_approximation == XC_KED_EXACT) then
      tau = states_batch_tau(states, nspin, m)
    else
      call functional_get_tau(xc_model%ked_functl, m, rho, rho_grad, rho_lapl, tau)
    end if
    if (xc_model%nlcc) then
      do i = 1, nspin
        rho(:, i)      = rho(:, i)      + xc_model%rho_core/real(nspin,R8)
        rho_grad(:, i) = rho_grad(:, i) + xc_model%grad_core/real(nspin,R8)
        rho_lapl(:, i) = rho_lapl(:, i) + xc_model%lapl_core/real(nspin,R8)
        tau(:, i)      = tau(:, i)      + xc_model%tau_core/real(nspin,R8)
      end do
    end if

    !Get ionization potential
    ip = states_batch_ip(states, nspin)
 
    !Get energy and potential
    do i = 1, 2
      call functional_get_vxc(xc_model%functls(i), m, rho, rho_grad, rho_lapl, &
                              tau, ip, v, e, vtau)
      if (present(exc)) then
        exc = exc + M_FOUR*M_PI*mesh_integrate(m, e)
      end if
      if (present(vxc)) then
        vxc = vxc + v
      end if
      if (present(vxctau)) then
        vxctau = vxctau + vtau
      end if

      if (iand(xc_model%corrections, XC_CORRECTION_RHOXC) /= 0 .and. present(vxc)) then
        if ( functional_kind(xc_model%functls(i)) == XC_EXCHANGE .or. &
             functional_kind(xc_model%functls(i)) == XC_EXCHANGE_CORRELATION) then
          allocate(dv(m%np, nspin))
          call functional_rhoxc(xc_model%functls(i), m, nspin, rho, rho_grad, rho_lapl, tau, ip, dv)
          vxc = vxc + dv
          deallocate(dv)
        end if
      end if
    end do

    if (iand(xc_model%corrections, XC_CORRECTION_ADSIC) /= 0) then
      call functional_adsic(xc_model%functls(1:2), m, nspin, rho, rho_grad, &
           rho_lapl, tau, ip, vaux, eaux)
      if (present(vxc)) then
        vxc = vxc - vaux
      end if
      if (present(exc)) then
        exc = exc - eaux
      end if

    end if

    !Deallocate arrays
    deallocate(rho, rho_grad, rho_lapl, tau, e, v, vaux)

    call pop_sub()
  end subroutine xc_potential

  subroutine xc_model_output_info(xc_model, unit, verbose_limit)
    !-----------------------------------------------------------------------!
    ! Returns the name of the exchange-correlation functionals.             !
    !-----------------------------------------------------------------------!
    type(xc_t), intent(in) :: xc_model
    integer,    intent(in), optional :: unit, verbose_limit

    integer :: i, n_message
    character(len=10) :: family
    character(len=80) :: name

    n_message = 2
    message(1) = ""
    message(2) = "Exchange-Correlation model:"

    do i = 1, 2
      select case (functional_family(xc_model%functls(i)))
        case (XC_FAMILY_LDA);  write(family,'(A)') "LDA"
        case (XC_FAMILY_GGA);  write(family,'(A)') "GGA"
        case (XC_FAMILY_MGGA); write(family,'(A)') "MGGA"
      end select

      select case (functional_kind(xc_model%functls(i)))
      case (XC_CORRELATION)
        n_message = n_message + 1
        write(message(n_message),'(2X,"Correlation: ",A," (",A,")")') &
                                   trim(functional_name(xc_model%functls(i))), trim(family)
      case (XC_EXCHANGE)
        n_message = n_message + 1
        write(message(n_message),'(2X,"Exchange:    ",A," (",A,")")') &
                                   trim(functional_name(xc_model%functls(i))), trim(family)
      case (XC_EXCHANGE_CORRELATION)
        n_message = n_message + 1
        write(message(n_message),'(2X,"Exchange-Correlation: ",A," (",A,")")') &
                                   trim(functional_name(xc_model%functls(i))), trim(family)
      end select
    end do

    if (iand(xc_model%corrections, XC_CORRECTION_REL_X) /= 0) then
      n_message = n_message + 1
      write(message(n_message),'(2X,"Relativistic corrections for Exchange")')
    end if
    if (iand(xc_model%corrections, XC_CORRECTION_ADSIC) /= 0) then
      n_message = n_message + 1
      write(message(n_message),'(2X,"Self-interaction correction: Averaged-Density SIC")')
    end if
    if (iand(xc_model%corrections, XC_CORRECTION_RHOXC) /= 0) then
      n_message = n_message + 1
      write(message(n_message),'(2X,"Exchange-correlation density correction")')
    end if

    if (xc_model%ked_approximation /= XC_KED_EXACT) then
      n_message = n_message + 1
      name = trim(functional_name(xc_model%ked_functl))
      write(message(n_message),'(2X,"KED approximation: ",A)') name(1:59)
    end if

   if (present(unit)) then
      call write_info(n_message, unit=unit)
    else
      if (present(verbose_limit)) then
        call write_info(n_message, verbose_limit)
      else
        call write_info(n_message)
      end if
    end if

  end subroutine xc_model_output_info

  subroutine xc_evaluate_ked_approximation(xc_model, m, states, nspin, unit)
    !-----------------------------------------------------------------------!
    !                                                                       !
    !  xc_model - exchange-correlation model                                !
    !  m        - mesh                                                      !
    !  states   - the states                                                !
    !  nspin    - number of spin channels                                   !
    !-----------------------------------------------------------------------!
    type(xc_t),           intent(in) :: xc_model
    type(mesh_t),         intent(in) :: m
    type(states_batch_t), intent(in) :: states
    integer,              intent(in) :: nspin
    integer,              intent(in) :: unit

    real(R8) :: ek_ks, ek_app
    real(R8), allocatable :: rho(:,:), rho_grad(:,:), rho_lapl(:,:), tau_app(:,:), tau_ks(:,:)

    call push_sub("xc_evaluate_ked_approximation")

    if (xc_model%ked_approximation == XC_KED_EXACT) then
      !Nothing do be done!
      call pop_sub()
      return
    end if

    !Compute Kohn-Sham densities
    allocate(rho(m%np, nspin), rho_grad(m%np, nspin), rho_lapl(m%np, nspin))
    allocate(tau_ks(m%np, nspin), tau_app(m%np, nspin))
    rho = states_batch_density(states, nspin, m)
    rho_grad = states_batch_density_grad(states, nspin, m)
    rho_lapl = states_batch_density_lapl(states, nspin, m)
    tau_ks = states_batch_tau(states, nspin, m)

    !Compute approximate KED
    call functional_get_tau(xc_model%ked_functl, m, rho, rho_grad, rho_lapl, tau_app)

    !Get kinetic energy
    ek_ks  = M_TWO*M_PI*mesh_integrate(m, sum(tau_ks, dim=2))
    ek_app = M_TWO*M_PI*mesh_integrate(m, sum(tau_app, dim=2))

    !Output
    write(message(1),'(2X,"KED Quality Factor: ",F10.3)') M_TWO*M_PI*mesh_integrate(m, sum(abs(tau_ks - tau_app),dim=2))/ek_ks
    if (any(tau_app < M_ZERO)) then
      message(2) = "  KED Negative Values: Yes"
    else
      message(2) = "  KED Negative Values: No"
    end if
    write(message(3),'(2X,"Kinetic Energy [",A,"]:",3X,"Kohn-Sham",5X,"Approximate",3X,"Error (%)")') trim(units_out%energy%abbrev)
    write(message(4),'(2X,17X,F14.6,2X,F14.6,3X,F6.2)') ek_ks, ek_app, (ek_app - ek_ks)/ek_ks*100
    call write_info(4,20)
    call write_info(4,unit=unit)

    deallocate(rho, rho_grad, rho_lapl, tau_app, tau_ks)

    call pop_sub()
  end subroutine xc_evaluate_ked_approximation

  subroutine xc_output(xc_model, m, states, nspin, dir)
    !-----------------------------------------------------------------------!
    ! Given a set of states, outputs the the corresponding                  !
    ! exchange-correlation potentials and energies.                         !
    !                                                                       !
    !  xc_model - exchange-correlation model                                !
    !  m        - mesh                                                      !
    !  states   - the states                                                !
    !  nspin    - number of spin channels                                   !
    !-----------------------------------------------------------------------!
    type(xc_t),           intent(in) :: xc_model
    type(mesh_t),         intent(in) :: m
    type(states_batch_t), intent(in) :: states
    integer,              intent(in) :: nspin
    character(len=*),     intent(in) :: dir

    integer  :: if, p, i, unit
    character(20) :: filename
    character(3) :: spin
    character(2) :: type
    type(xc_t) :: xc_tmp
    real(R8), allocatable :: e(:), v(:,:), tau(:,:), vtau(:,:)

    call push_sub("xc_output")

    !Create a temporary xc model
    call xc_null(xc_tmp)
    xc_tmp = xc_model
    do if = 1, 3
      call functional_end(xc_tmp%functls(if))
      call functional_init(nspin, 0, 0, xc_tmp%functls(if))
    end do

    !Allocate memory
    allocate(v(m%np, nspin), e(m%np), vtau(m%np, nspin))

    !Loop over the functionals
    do if = 1, 2
      if (functional_kind(xc_model%functls(if)) < 0) cycle

      !Get the potential
      xc_tmp%functls(1) = xc_model%functls(if)
      call xc_potential(xc_tmp, m, states, nspin, vxc=v, vxctau=vtau)

      do p = 1, nspin
        !Get the filename
        select case (functional_kind(xc_model%functls(if)))
        case (XC_EXCHANGE)
          type = "x "
        case (XC_CORRELATION)
          type = "c "
        case (XC_EXCHANGE_CORRELATION)
          type = "xc"
        end select
        if (nspin == 2) then
          if (p == 1) then
            spin = "_dn"
          elseif (p == 2) then
            spin = "_up"
          end if
        else
          spin = ""
        end if
        filename = "v_"//trim(type)//trim(spin)

        !Open file
        call io_open(unit, file=trim(dir)//"/"//trim(filename))

        !Write header
        write(unit,'("# ")')
        write(unit,'("# Energy units: ",A)') trim(units_out%energy%name)
        write(unit,'("# Length units: ",A)') trim(units_out%length%name)
        write(unit,'("#")')
        write(unit,'("# ",36("-"))')
        write(unit,'("# |",8X,"r",7X,"|",7X,"v(r)",6X,"|")')
        write(unit,'("# ",36("-"))')

        !Ouput
        do i = 1, m%np
          write(unit,'(4(3X,ES15.8E2))') m%r(i)/units_out%length%factor, &
                                         v(i, p)/units_out%energy%factor
        end do

        close(unit)
        
        !MGGA term
        if (functional_family(xc_model%functls(if)) == XC_FAMILY_MGGA) then
          filename = "vtau_"//trim(type)//trim(spin)

          !Open file
          call io_open(unit, file=trim(dir)//"/"//trim(filename))
          
          !Write header
          write(unit,'("# ")')
          write(unit,'("# Energy units: ",A)') trim(units_out%energy%name)
          write(unit,'("# Length units: ",A)') trim(units_out%length%name)
          write(unit,'("#")')
          write(unit,'("# ",36("-"))')
          write(unit,'("# |",8X,"r",7X,"|",7X,"v(r)",6X,"|")')
          write(unit,'("# ",36("-"))')

          !Ouput
          do i = 1, m%np
            write(unit,'(4(3X,ES15.8E2))') m%r(i)/units_out%length%factor, &
                 vtau(i, p)/units_out%energy%factor
          end do

          close(unit)
        end if

      end do
    end do

    !Deallocate memory
    deallocate(v, e, vtau)
    call xc_end(xc_tmp)

    !NLCC
    if (xc_model%nlcc) then
      call io_open(unit, file=trim(dir)//"/core_correction")

      write(unit,'("#")')
      write(unit,'("# Radial core correction density and derivatives.")')
      write(unit,'("# Length units: ",A)') trim(units_out%length%name)
      write(unit,'("#")')

      write(unit,'("# ",88("-"))')
      write(unit,'("# |",7X,"r",7X,"|",6X,"n(r)",7X,"|",4X,"grad_n(r)",4X,"|",4X,"lapl_n(r)",4X,"|",5X,"tau(r)",6X,"|")')
      write(unit,'("# ",88("-"))')
      do i = 1, m%np
        write(unit,'(3X,ES14.8E2,3X,ES15.8E2,3X,ES15.8E2,3X,ES15.8E2,3X,ES15.8E2)') &
             m%r(i)/units_out%length%factor, &
             xc_model%rho_core(i)*units_out%length%factor, &
             xc_model%grad_core(i)*units_out%length%factor**2, &
             xc_model%lapl_core(i)*units_out%length%factor**3, &
             xc_model%tau_core(i)*units_out%length%factor/units_out%energy%factor
      end do

      close(unit)

    end if

    if (xc_model%ked_approximation /= XC_KED_EXACT) then
      
      allocate(tau(m%np, nspin))
      call functional_get_tau(xc_model%ked_functl, m, &
           states_batch_density(states, nspin, m), &
           states_batch_density_grad(states, nspin, m), &
           states_batch_density_lapl(states, nspin, m), tau)

      do p = 1, nspin
        if (nspin == 1) then
          filename = trim(dir)//"/app_tau"
        else
          if (p == 1) then
            filename = trim(dir)//"/app_tau_dn"
          elseif (p == 2) then
            filename = trim(dir)//"/app_tau_up"
          end if
        end if
        call io_open(unit, file=trim(filename))

        write(unit,'("#")')
        write(unit,'("# Radial approximated kinetic energy density.")')
        write(unit,'("# Length units: ",A)') trim(units_out%length%name)
        write(unit,'("# Energy units: ",A)') trim(units_out%energy%name)
        write(unit,'("#")')

        write(unit,'("# ",35("-"))')
        write(unit,'("# |",7X,"r",7X,"|",5X,"tau(r)",6X,"|")')
        write(unit,'("# ",35("-"))')
        do i = 1, m%np
          write(unit,'(3X,ES14.8E2,3X,ES15.8E2)') m%r(i)/units_out%length%factor, &
                                 tau(i, p)*units_out%length%factor/units_out%energy%factor
        end do

        close(unit)
      end do
      
      deallocate(tau)
    end if

    call pop_sub()
  end subroutine xc_output

  subroutine xc_ps_io_set(xc_model)
    !-----------------------------------------------------------------------!
    ! Pass the information about the exchange-correlation model to the      !
    ! ps_io module.                                                         !
    !-----------------------------------------------------------------------!
    type(xc_t), intent(in) :: xc_model

    call push_sub("xc_ps_io_set")

    call ps_io_set_xc(xc_model%id_val)
    
    if (xc_model%nlcc) then
      call ps_io_set_nlcc(xc_model%rc, xc_model%np, xc_model%rho_core, xc_model%tau_core)
    end if

    call pop_sub()
  end subroutine xc_ps_io_set

end module xc_m
