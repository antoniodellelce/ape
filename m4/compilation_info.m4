## Copyright (C) 2010 The octopus team
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##
## $Id: compilation_info.m4 778 2013-07-11 15:49:39Z micael $
##

################################################
# Generates macro definitions for:
#  LATEST_SVN : the svn revision number.
#  BUILD_TIME : when the configure script is launched.
#  FC : The Fortran compiler
#  CC : The C compiler
#  CFLAGS : The flags passed to the C compiler.
#  FCFLAGS : The flags passed to the Fortran compiler.
# ----------------------------------
AC_DEFUN([ACX_COMPILATION_INFO],
[
AC_MSG_NOTICE([collecting compilation info...])

rev=$(sh ./build/svn_release_number.sh 2> /dev/null)
date=`date`
AC_MSG_NOTICE([collecting compilation info... "$rev"])

AC_DEFINE_UNQUOTED([LATEST_SVN], ["$rev"], [subversion revision number])
AC_DEFINE_UNQUOTED([BUILD_TIME], ["$date"], [date when configure was launched])
AC_DEFINE_UNQUOTED([CC], ["$CC"], [C compiler])
AC_DEFINE_UNQUOTED([FC], ["$FC"], [Fortran compiler])
AC_DEFINE_UNQUOTED([CFLAGS], ["$CFLAGS"], [C compiler])
AC_DEFINE_UNQUOTED([FCFLAGS], ["$FCFLAGS $FCFLAGS_f90"], [Fortran compiler])

]
)
